'''wal-g server entry'''

import os
import logging
from subprocess import Popen

from fastapi import FastAPI, HTTPException
from fastapi.logger import logger as app_logger

logging.basicConfig(level=logging.DEBUG)

PGDATA = os.getenv('PGDATA', '/var/lib/postgresql/data')
PGWAL = os.getenv('PGWAL', f'{PGDATA}/pg_wal')

WALG_BIN = os.getenv('WALG_BIN', 'wal-g')
WALG_COMMAND_FLAGS = os.getenv('WALG_COMMAND_FLAGS', '')

def perform_command(command: str):
    '''general function to exec command'''
    process = Popen(command)

    outputs, errors = process.communicate()

    if process.returncode == 0:
        return outputs, 200

    return errors, 500

def walg_push(segment_id):
    '''save wal to storage'''
    command = [WALG_BIN, 'wal-push', f'{PGWAL}/{segment_id}']

    if len(WALG_COMMAND_FLAGS) > 0:
        for f in WALG_COMMAND_FLAGS.split():
            command.append(f)

    app_logger.info(f'wal pushing - {command}')

    return perform_command(command)

def walg_fetch(segment_path, segment_id):
    '''fetch wal from storage'''
    command = [WALG_BIN, 'wal-fetch', segment_path, f'{PGWAL}/{segment_id}']

    if len(WALG_COMMAND_FLAGS) > 0:
        for f in WALG_COMMAND_FLAGS.split():
            command.append(f)

    app_logger.info(f'wal fetching - {command}')

    return perform_command(command)

def walg_backup_push():
    '''backup to storage'''
    command = [WALG_BIN, 'backup-push', PGDATA]

    if len(WALG_COMMAND_FLAGS) > 0:
        for f in WALG_COMMAND_FLAGS.split():
            command.append(f)

    app_logger.info(f'backup pushing - {command}')

    return perform_command(command)

def walg_backup_fetch(backup_name = 'LATEST'):
    '''restore point in time backup by its id'''
    command = [WALG_BIN, 'backup-fetch', PGDATA, backup_name]

    if len(WALG_COMMAND_FLAGS) > 0:
        for f in WALG_COMMAND_FLAGS.split():
            command.append(f)

    app_logger.info(f'backup fetching - {command}')

    return perform_command(command)

def main():
    '''init fast api and serve walg api'''
    app = FastAPI()

    @app.get('/health', status_code=201)
    def health():
        '''simple health check endpoint'''
        pass

    @app.get('/wal-push/{segment_path:path}', status_code=201)
    def wal_push(segment_path: str):
        '''save wal segment to storage by database software'''
        res, status_code = walg_push(segment_path.split('/')[-1])

        app_logger.debug(res)

        if status_code == 500:
            raise HTTPException(status_code=status_code, detail='')

    @app.get('/wal-fetch/{segment_path:path}', status_code=201)
    def wal_fetch(segment_path: str):
        '''fetch wal segment from storage by database software'''
        res, status_code = walg_fetch(segment_path, segment_path.split('/')[-1])

        app_logger.debug(res)

        if status_code == 500:
            raise HTTPException(status_code=status_code, detail='')

    @app.get('/backup-push', status_code=201)
    def backup_push():
        '''manual back up trigger to save database backup to storage'''
        res, status_code = walg_backup_push()

        app_logger.debug(res)

        if status_code == 500:
            raise HTTPException(status_code=status_code, detail='')

    @app.get('/backup-fetch', status_code=201)
    def backup_fetch(backup_name: str = 'LATEST'):
        '''manual restore trigger to fetch database backup from storage'''
        res, status_code = walg_backup_fetch(backup_name)

        app_logger.debug(res)

        if status_code == 500:
            raise HTTPException(status_code=status_code, detail='')

    app_logger.info('server starting')

    return app

app = main()
