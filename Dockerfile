FROM alpine:3.13

ARG walg_version=v0.2.19

ENV GOPATH=/go GOBIN=/usr/local/bin USE_LIBSODIUM=true USE_BROTLI=true

RUN apk add --no-cache --virtual .build-deps build-base libsodium-dev git go cmake curl bash &&\
  go get github.com/wal-g/wal-g || true &&\
  cd $GOPATH/src/github.com/wal-g/wal-g &&\
  git checkout $walg_version &&\
  make install &&\
  make deps &&\
  make pg_build &&\
  mv $GOPATH/src/github.com/wal-g/wal-g/main/pg/wal-g /usr/local/bin/ &&\
  apk add --no-cache --virtual .deps libsodium python3 py3-pip &&\
  ln -s $(which python3) /usr/local/bin/python &&\
  apk del .build-deps &&\
  rm -rf ~/.cache/* &&\
  rm -rf $GOPATH/src/* $GOPATH/pkg/*

WORKDIR /opt/app

COPY . .

RUN pip install --no-cache-dir -r requirements.txt

CMD ["hypercorn", "server:app", "--bind=0.0.0.0:80"]
