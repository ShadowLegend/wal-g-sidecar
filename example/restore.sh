#!/bin/bash

# for more info about recovery step
# check: https://www.postgresql.org/docs/12/continuous-archiving.html

pg_base_data_dir=/var/lib/postgresql/data
pgwal_dir=$pg_base_data_dir/pg_wal
pgdata_dir=$pg_base_data_dir/pg_data

docker container rm -f database walg
docker volume rm -f example_database

docker-compose up --no-start

docker container cp database:/etc/passwd .
ids=($(cat passwd | grep -o '^postgres:.*' | sed 's/^postgres:x:\([0-9]*:[0-9]*\):.*/\1/g' | tr ':' ' '))
pg_user_id=${ids[0]}
pg_group_id=${ids[1]}
rm passwd

docker container start walg

until [[ "$(curl -s -o /dev/null -w ''%{http_code}'' 'localhost:7000/backup-fetch?backup_name=LATEST')" == "201" ]]; do sleep 2; done

docker container exec walg touch $pgdata_dir/recovery.signal
docker container exec walg sh -c "sed -i \"s#^\#restore_command =.*#restore_command='/usr/bin/wget -O /dev/null -o /dev/null walg/wal-fetch/%f'#\" $pgdata_dir/postgresql.conf"
docker container exec walg mkdir $pgwal_dir
docker container exec walg ln -s $pgwal_dir $pgdata_dir/pg_wal
docker container exec walg chown -R $pg_user_id:$pg_group_id $pg_base_data_dir

docker container start database
