
create table users (
  id serial primary key,
  name varchar(128) not null unique,
  inserted_at timestamp not null default current_timestamp
);

create table articles (
  id serial primary key,
  author_id int not null,
  title varchar(128) not null unique,
  description varchar(255) not null,
  inserted_at timestamp not null default current_timestamp,
  foreign key (author_id) references users(id)
);

insert into users (name) values ('Bobine');
insert into users (name) values ('Dani');
insert into users (name) values ('Fayina');
insert into users (name) values ('Avrom');
insert into users (name) values ('Courtney');
insert into users (name) values ('Roseline');
insert into users (name) values ('Sada');
insert into users (name) values ('Max');
insert into users (name) values ('Zacharie');
insert into users (name) values ('Evangelin');
insert into users (name) values ('Leonanie');
insert into users (name) values ('Catarina');
insert into users (name) values ('Scot');
insert into users (name) values ('Abra');
insert into users (name) values ('Randie');
insert into users (name) values ('Kerry');

insert into articles (title, description, author_id) values ('Swiss Family Robinson', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.', 8);
insert into articles (title, description, author_id) values ('Notes on a Scandal', 'Suspendisse potenti.', 2);
insert into articles (title, description, author_id) values ('Everyday People', 'Praesent id massa id nisl venenatis lacinia.', 6);
insert into articles (title, description, author_id) values ('Oliver!', 'Nam dui.', 4);
insert into articles (title, description, author_id) values ('Paradise Lost 2: Revelations', 'Morbi a ipsum.', 4);
insert into articles (title, description, author_id) values ('Ghost and the Darkness, The', 'Maecenas leo odio, condimentum id, luctus nec, molestie sed, justo.', 2);
insert into articles (title, description, author_id) values ('Josephine', 'Donec dapibus.', 16);
insert into articles (title, description, author_id) values ('Prometheus', 'Duis mattis egestas metus.', 14);
insert into articles (title, description, author_id) values ('Experience Preferred... But Not Essential', 'Nam congue, risus semper porta volutpat, quam pede lobortis ligula, sit amet eleifend pede libero quis orci.', 4);
insert into articles (title, description, author_id) values ('Story of Science, The', 'Vestibulum ac est lacinia nisi venenatis tristique.', 11);
insert into articles (title, description, author_id) values ('Dangerous Liaisons', 'In hac habitasse platea dictumst.', 2);
insert into articles (title, description, author_id) values ('Alexander''s Ragtime Band', 'Mauris ullamcorper purus sit amet nulla.', 10);
insert into articles (title, description, author_id) values ('Ghoulies', 'Duis bibendum, felis sed interdum venenatis, turpis enim blandit mi, in porttitor pede justo eu massa.', 11);
insert into articles (title, description, author_id) values ('My Lady Margarine (Die Austernprinzessin) (Oyster Princess, The)', 'Nulla suscipit ligula in lacus.', 10);
insert into articles (title, description, author_id) values ('Midnight in Paris', 'Integer ac leo.', 13);
insert into articles (title, description, author_id) values ('Bummer (Bumer)', 'Etiam faucibus cursus urna.', 12);
