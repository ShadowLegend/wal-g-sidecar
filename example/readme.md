
### how to run example

prequisite:
  - `docker` installed

1. first create an account for aws
    1. create root account
    2. create one user with programmatic access and note down credentials
    3. create permission to get/put/delete and list object to bucket with the name `wal-g-example`
    4. assign permission to the user
    5. create s3 bucket with the name `wal-g-example`
    6. update bucket policy to allow the user to list object (get/put/delete are allowed through iam policy on step 1.3 and 1.4)
2. export environment variable accordingly to `.env.example`
3. run `docker-compose up -d`
4. exec into `database` container to use `sample.sql` (it is possible to run it one line (eg: `... exec database bash -c '...'`))
5. testing
    1. check s3 bucket to see any wal being stored
    2. test base backup by doing `curl localhost:7000/backup-push` to push base backup to s3 bucket
    3. do only step `4` again but use `test.sql` to insert additional data
    4. access `psql` shell and try `select count(*) from users` to see if database has 32 users
    5. run `restore.sh` script to perform database restore
    6. do step `5.4` again and you will see only 16 users back
6. that's all it is for `example`, have fun and try around with different cases and parameters
